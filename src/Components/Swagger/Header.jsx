import MonCSS from "./Swagger.module.css"

const Header=function(props)
{
    return(
        <div className={MonCSS.wrapper}>
            <section className={[MonCSS.block,MonCSS.col-12]}>
                <div>
                    <div className={MonCSS.info}>
                        <hgroup className={MonCSS.main}>
                            <h2 className={MonCSS.title}>
                                React Challenge
                            </h2>
                            <a target="_blank" href="./datas/swagger.json" rel="noopener noreferrer" className={MonCSS.link}>
                                <span className={MonCSS.url}>swagger.json</span>
                            </a>
                        </hgroup>
                        <div className={MonCSS.description}>
                            <div className={MonCSS.markdown}>
                                <p>React swagger clone</p>
                            </div>
                        </div> 
                        <div className={MonCSS.info__contact}>
                            <a href="mailto:michael.person@cognitic.be" rel="noopener noreferrer" className={MonCSS.link}>
                                Contact the developer
                            </a>
                        </div>
                        <div className={MonCSS.info__license}>
                            <a target="_blank" href="http://www.apache.org/licenses/LICENSE-2.0.html" rel="noopener noreferrer" className={MonCSS.link}>
                                Apache 2.0
                            </a>
                        </div>
                        <a className={[MonCSS.info__extdocs,MonCSS.link,MonCSS.info]} target="_blank" href="http://swagger.io" rel="noopener noreferrer">Find out more about Swagger</a>
                    </div>
                </div>
            </section>  
        </div>
    );
};

export default Header;