import React from "react";
import styles from "./Swagger.module.css"
import Verbs from "./Verbs";
import shortid from 'shortid';
import swag from "../../datas/SwaggerApi.json";

const Tableau=function(props)
{
    return(
        <section className={[styles.block,styles.col-12]}>
    <div>
        <span>
            <div className={[styles.opblocktagsection, styles.isopen]}>
                <h4 className={styles.opblocktag}><a className={styles.nostyle}
                        href="#/Foo"><span>Foo</span></a><small> 
                    </small> 
                </h4>  
                <div className={styles.nomargin}>    
                {swag.map((data) => (
                                    <Verbs key={shortid.generate()}  info={data} />
                ))}              
                         
                </div>             
            </div>
        </span>
    </div>
</section>
    );
}

export default Tableau;