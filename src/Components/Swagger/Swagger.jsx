import React from "react"; 
import Topbar from "./topbar";
import Header from "./Header";
import Tableau from "./Tableau";
import styles from "./Swagger.module.css"

const Swagger= function(props)
{
    return(<div id="swagger-id">
                <section className={styles.swaggerUi}>
                    <Topbar/>
                    <div className={styles.swaggerUi}>
                        <div>
                            <Header/>
                        </div>
                    </div>
                    <Tableau/>
                </section>
            </div>);
};

export default Swagger;