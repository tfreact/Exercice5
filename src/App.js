 
import './App.css';
import Swagger from './Components/Swagger/Swagger';

function App() {
  return (
    <div className="App">
       <Swagger/>
    </div>
  );
}

export default App;
